# Vodafone DE ETO Front-End Project

* This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.5.
* You will need Node to be previous installed, so please download the latest version of [Node](https://nodejs.org/en/download/).
* We use [SASS](https://sass-lang.com/) for styling our portal. To perform the installtion please run the following commands `npm install -g sass`.

## Development server

Run `npm run dev` for a dev server and build vfde-eto-esp-common library to `dist/` folder. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files. This path only have Front-End.

## Build / Development Server with Backend

Run `npm run build` to build the project. The build artifacts will be stored in the `public/` directory inside backend project. Navigate to `http://localhost:3000/`. Use the `--prod` flag for a production build.

## Build vfde-eto-esp-common library

Run `npm run packagr` to build vfde-eto-esp-common library to `dist/` folder.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Running unit tests

Run `npm run test` to execute the unit tests via [Jest](https://jestjs.io/).
Run `npm run test:coverage`. This indicates that test coverage information should be collected and reported in the output.
Run `npm run test:snapshot`. Test coverage and snapshot information should be collected and reported in the output.
Run `npm run test:watch`. Watch files for changes and rerun all tests when something changes.

## Running end-to-end tests

Run `npm run e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

### Clone

- Clone this repo to your local machine using `https://user@bitbucket.org/celfocus/cf-vfde-eto-esp-fe.git`
