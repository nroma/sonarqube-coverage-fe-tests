import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
 /*
  * Please order the components alphabetically
 */
import { Accordion } from './components/accordion/accordion.component';
import { AccordionList } from './components/accordion-list/accordion-list.component';
import { AlertBox } from './components/alert-box/alert-box.component';
import { Button } from './components/button/button.component';
import { Dropdown } from './components/dropdown/dropdown.component';
import { FormInput } from './components/input/input.component';
import { FormLabel } from './components/label/label.component';
import { Heading } from './components/heading/heading.component';
import { Panel } from './components/panel/panel.component';
import { TableCell } from './components/table-cell/table-cell.component';
import { TitleBlock } from './components/title-block/title-block.component';
import { Text, TextInline, TextBold, TextLegalName } from './components/text/text.component';
import { Tooltip } from './components/tooltip/tooltip.component';

@NgModule({
    declarations: [
        Accordion,
        AccordionList,
        AlertBox,
        Button,
        Dropdown,
        FormInput,
        FormLabel,
        Heading,
        Panel,
        TableCell,
        Text,
        TextInline,
        TextBold,
        TextLegalName,
        TitleBlock,
        Tooltip
    ],
    imports: [
        CommonModule,
        TranslateModule.forRoot()
    ],
    exports: [
        Accordion,
        AccordionList,
        AlertBox,
        Button,
        Dropdown,
        FormInput,
        FormLabel,
        Heading,
        Panel,
        TableCell,
        Text,
        TextInline,
        TextBold,
        TextLegalName,
        TitleBlock,
        Tooltip
    ]
})
export class VfdeEtoEspCommonModule { }
