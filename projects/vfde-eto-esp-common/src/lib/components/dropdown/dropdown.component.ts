import { Component, Input, Output, EventEmitter, HostBinding } from '@angular/core';

export class DropdownValue {
    label: string;
    value: string;

    constructor(label: string, value: string) {
        this.label = label;
        this.value = value;
    }
}

@Component({
  selector: 'vf-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
})

export class Dropdown {
    @Input() placeholder: string;
    @Input() values: DropdownValue[];
    @Input() toggle: boolean;
    @Input() titleId: boolean;
    @Input() valueId: boolean;
    @Input() zIndex: number;
    @Output() select: EventEmitter<any>;

    @HostBinding('class') class = 'dropdown__item';

    constructor() {
        this.select = new EventEmitter();
    }

    selectItem(label: string, value: string) {
        this.select.emit(label);
        this.select.emit(value);
        alert("Type: " + label + '\n' + "Sim Number: " + value);

    }

    toggleDropdown() {
        this.toggle = !this.toggle;
    }
}
