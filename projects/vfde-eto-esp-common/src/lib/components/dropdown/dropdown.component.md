# Dropdown Component ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

This is a tutorial for the use of Dropdown Component

## Usage

```python
import { Dropdown } from './url'

<vf-dropdown /> # returns the component
```
## Input and Output attributes plus Event Handlers

| Name | Type | Default | Description |
| ---- | ---- | ------- | ----------- |
| placeholder | string | empty | Sets a value in the dropdown until a selection is made |
| titleId | string | empty | Defines the titleId of the option |
| valueId | string | empty | Defines the valueId of the option |
| values | array | empty | Compose with objects to define values of each option |
| zIndex | string | empty | Defines the zIndex of the dropdown |
| click | event | empty | Fires a click event |
