import { Component, HostBinding } from '@angular/core';

@Component({
    selector: 'vf-table-cell',
    templateUrl: './table-cell.component.html',
    styleUrls: ['./table-cell.component.scss']
})

export class TableCell {
    @HostBinding('class') class = 'chevron__text';
}
