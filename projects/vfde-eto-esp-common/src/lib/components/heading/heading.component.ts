import { Component, Input } from '@angular/core';


@Component({
    selector: 'vf-heading',
    templateUrl: './heading.component.html',
    styleUrls: ['./heading.component.scss'],
})
export class Heading {
    @Input() type: string;
    @Input() text: string;
    @Input() noGutter: boolean;
}
