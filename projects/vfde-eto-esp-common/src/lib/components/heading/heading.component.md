# Heading Component ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

This is a tutorial for the use of Heading Component

## Usage

```python
import { Heading } from './url'

<vf-heading /> # returns the component
```
## Input and Output attributes

| Name | Type | Default | Description |
| ---- | ---- | ------- | ----------- |
| type | one of: '1', '2', '3', '4', '5', '6' | empty | Defines the type of the heading |
| text | string | empty | Defines the text of the heading |

## Select Attributes

| Name | Type | Default | Description |
| ---- | ---- | ------- | ----------- |
| headingX | attr | empty | Points the content to the ng-content in the headingX |
