# Panel Component ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

This is a tutorial for the use of Panel Component

## Usage

```python
import { Panel } from './url'

<vf-panel /> # returns the component
```
## Input and Output attributes plus Event Handlers

| Name | Type | Default | Description |
| ---- | ---- | ------- | ----------- |
| identifier | string | empty | Defines the Id of the component |
