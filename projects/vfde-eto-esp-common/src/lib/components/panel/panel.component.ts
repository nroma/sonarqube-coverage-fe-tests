import { Component, Input, HostBinding } from '@angular/core';

@Component({
  selector: 'vf-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss'],
})

export class Panel {
    @Input() identifier: string;

    @HostBinding('class') class = "panel__item";
}
