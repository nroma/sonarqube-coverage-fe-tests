import { Component, Input, Directive, HostBinding } from '@angular/core';

@Directive({
    selector: '[inline]'
})
export class TextInline {
    @HostBinding('class') class = "text--inline";
}

@Directive({
    selector: '[bold]'
})
export class TextBold {
    @HostBinding('class') class = "text--bold";
}

@Directive({
    selector: '[legalName]'
})
export class TextLegalName {
    @HostBinding('class') class = "text--legal-name";
}

@Component({
    selector: 'vf-text',
    templateUrl: './text.component.html',
    styleUrls: ['./text.component.scss']
})

export class Text {
    @Input() title: string;

    @HostBinding('class') class = "text";
}
