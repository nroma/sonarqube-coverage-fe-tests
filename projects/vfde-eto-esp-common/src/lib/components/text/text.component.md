# Text Component ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

This is a tutorial for the use of Text Component

## Usage

```python
import { Text, TextInline, TextBold, TextLegalName } from './url'

<vf-text /> # returns the component
```
## Input and Output attributes

| Name | Type | Default | Description |
| ---- | ---- | ------- | ----------- |
| title | string | empty | - |
| role | one of: 'button', 'link' | empty | Defines the role of the button |

## Directives

| Selector | Type | Default | Description |
| ---- | ---- | ------- | ----------- |
| inline | boolean | false | Binds an inline class to the element |
| bold | boolean | false | Binds a bold class to the element |
| legalName | boolean | false | Binds a legalName class to the element |
