# Accordion Component ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

This is a tutorial for the use of Accordion Component

## Usage

```python
import { Accordion } from './url'

<vf-accordion /> # returns the component
```
## Input and Output attributes plus Event Handlers

| Name | Type | Default | Description |
| ---- | ---- | ------- | ----------- |
| identifier | string | empty | Defines the Id of the accordion |
| role | one of: 'button', 'link' | empty | Defines the role of the button |
| state | one of: 'open', 'closed' | 'closed' | Defines the state of the the accordion |
| click | event | empty | Fires a click event |

## Select Attributes

| Name | Type | Default | Description |
| ---- | ---- | ------- | ----------- |
| accordion-header | attr | empty | Points the content to the ng-content in the title |
| accordion-body | attr | empty | Points the content to the ng-content in the body |
