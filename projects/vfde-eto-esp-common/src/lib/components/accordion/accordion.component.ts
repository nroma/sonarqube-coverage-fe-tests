import { Component, Input } from '@angular/core';
import { slideDown } from '../../animations/slideDown';

@Component({
  selector: 'vf-accordion',
  animations: [
    slideDown
  ],
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss'],
})

export class Accordion {
    @Input() identifier: string;
    @Input() state: string = 'closed';

    booleanState = false;

    toggleState() {
        if(this.state == 'closed') {
            this.state = this.state === 'closed' ? 'open' : 'closed';
        } else {
            this.state = this.state === 'open' ? 'closed' : 'open';
        }

        this.booleanState = !this.booleanState;
    }

    constructor() { }
}
