import { Component, Input, HostBinding } from '@angular/core';

@Component({
  selector: 'vf-alert-box',
  templateUrl: './alert-box.component.html',
  styleUrls: ['./alert-box.component.scss'],
})

export class AlertBox {
    @Input() identifier: string;

    @HostBinding('class') class = 'alert-box__item';

    constructor() { }
}
