# Alert Box Component ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

This is a tutorial for the use of Alert Box Component

## Usage

```python
import { AlertBox } from './url'

<vf-alert-box /> # returns the component
```
## Input and Output attributes

| Name | Type | Default | Description |
| ---- | ---- | ------- | ----------- |
| identifier | string | empty | Defines the Id of the alert-box |
