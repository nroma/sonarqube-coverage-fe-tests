# Tooltip Component ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

This is a tutorial for the use of Tooltip Component

## Usage

```python
import { Tooltip } from './url'

<vf-tooltip /> # returns the component
```
## Input and Output attributes plus Event Handlers

| Name | Type | Default | Description |
| ---- | ---- | ------- | ----------- |
| id | string | empty | Defines the Id to the tooltip |
| click | event | empty | Fires a click event |
