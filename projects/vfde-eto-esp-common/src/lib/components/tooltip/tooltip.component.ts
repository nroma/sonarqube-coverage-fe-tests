import { Component, Input } from '@angular/core';

@Component({
  selector: 'vf-tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.scss']
})

export class Tooltip {
    @Input() toggle: boolean;
    @Input() id: string;

    close(event) {
        event.preventDefault();
        this.toggle = false;
    }
}
