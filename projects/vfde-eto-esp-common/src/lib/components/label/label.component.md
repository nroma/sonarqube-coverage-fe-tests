# Form Label Component ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

This is a tutorial for the use of Form Label Component

## Usage

```python
import { FormLabel } from './url'

<vf-label /> # returns the component
```
## Input and Output attributes

| Name | Type | Default | Description |
| ---- | ---- | ------- | ----------- |
| title | string | empty | Defines the title of the label |
| for | string | empty | This needs to match with the value of the respective input ID attribute |
