import { Component, Input } from '@angular/core';

@Component({
  selector: 'vf-label',
  templateUrl: './label.component.html',
  styleUrls: ['./label.component.scss'],
})

export class FormLabel {
    @Input() title: string;
    @Input() for: string;

    constructor() { }
}
