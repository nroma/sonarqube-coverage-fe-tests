# Accordion List Component ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

This is a tutorial for the use of Accordion List Component

## Usage

```python
import { AccordionList } from './url'

<vf-accordion-list /> # returns the component
```
## Input and Output attributes plus Event Handlers

| Name | Type | Default | Description |
| ---- | ---- | ------- | ----------- |
| msisdn | string | empty | - |
| identifier | string | empty | Defines the Id of the accordion |
| tooltipIdentifier | string | empty | Defines the Id of the tooltip |
| state | one of: 'open', 'closed' | 'closed' | Defines the state of the the accordion |
| click | event | empty | Fires a click event |

## Select Attributes

| Name | Type | Default | Description |
| ---- | ---- | ------- | ----------- |
| accordion-list-header | attr | empty | Points the content to the ng-content in the title |
| accordion-list-body | attr | empty | Points the content to the ng-content in the body |
