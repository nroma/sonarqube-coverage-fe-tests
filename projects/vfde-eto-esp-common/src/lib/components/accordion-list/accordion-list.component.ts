import { Component, Input, HostBinding, Output, EventEmitter, ViewChild } from '@angular/core';
import { slideDown } from '../../animations/slideDown';
import { Tooltip } from '../tooltip/tooltip.component';

@Component({
    selector: 'vf-accordion-list',
    animations: [
        slideDown
    ],
    templateUrl: './accordion-list.component.html',
    styleUrls: ['./accordion-list.component.scss']
})

export class AccordionList {
    @Input() identifier: string;
    @Input() tooltipIdentifier: string;
    @Input() state: string = 'closed';
    @Input() disabled: boolean = false;
    @Output() onClick = new EventEmitter<any>();

    @HostBinding('class.accordion-list--border-bottom') isClosed: boolean = true;

    @ViewChild(Tooltip)
    tooltip: Tooltip;

    toggleState() {
        if (this.state == 'closed') {
            this.state = this.state === 'closed' ? 'open' : 'closed';
        } else {
            this.state = this.state === 'open' ? 'closed' : 'open';
        }

        this.isClosed = !this.isClosed;
    }

    onClickInfo(event) {
        this.onClick.emit(event);
    }

    showTooltip(event) {
        event.preventDefault();
        this.tooltip.toggle = true;
        this.tooltip.id = this.tooltipIdentifier;
    }
}
