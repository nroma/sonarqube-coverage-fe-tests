import { Component, HostBinding, Input, OnInit } from '@angular/core';

@Component({
    selector: 'vf-title-block',
    templateUrl: './title-block.component.html',
    styleUrls: ['./title-block.component.scss']
})

export class TitleBlock {
    @Input() color: string;
    @Input() title: string;
    @Input() id: string;
    @HostBinding('class') class = 'title-block';

    constructor () {}

    public isSilver = false;

    ngOnInit() {
        if(this.color == 'silver') {
            this.isSilver = true;
        } else {
            this.isSilver = false;
        }
    }
}
