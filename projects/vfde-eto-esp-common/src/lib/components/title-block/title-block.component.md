# Title Block Component ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

This is a tutorial for the use of Title Block Component

## Usage

```python
import { TitleBlock } from './url'

<vf-title-block /> # returns the component
```
## Input and Output attributes

| Name | Type | Default | Description |
| ---- | ---- | ------- | ----------- |
| color | one of: 'silver' | empty | Gives color to the title |
| title | string | empty | Defines the title to the title-block |
| id | string | empty | Defines the Id to child |
