# Input Component ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

This is a tutorial for the use of Input Component

## Usage

```python
import { FormInput } from './url'

<vf-input /> # returns the component
```
## Input and Output attributes

| Name | Type | Default | Description |
| ---- | ---- | ------- | ----------- |
| type | string | empty | Defines the type of the input |
| name | string | empty | Defines the name of the input |
| id | string | empty | Defines the Id of the input |
| placeholder | string | empty | Defines the placeholder of the input |
