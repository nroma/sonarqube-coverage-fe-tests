import { Component, Input } from '@angular/core';

@Component({
  selector: 'vf-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
})

export class FormInput {
    @Input() type: string;
    @Input() name: string;
    @Input() id: string;
    @Input() placeholder: string;

    constructor() { }
}
