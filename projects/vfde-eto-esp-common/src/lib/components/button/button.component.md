# Button Component ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

This is a tutorial for the use of Button Component

## Usage

```python
import { Button } from './url'

<vf-button /> # returns the component
```
## Input and Output attributes plus Event Handlers

| Name | Type | Default | Description |
| ---- | ---- | ------- | ----------- |
| type | one of: 'primary', 'secondary', 'tertiary', 'plain' | empty | Defines the type of the button |
| id | string | empty | - |
| role | one of: 'button', 'link' | empty | Defines the role of the button |
| full_width | boolean | true | Sets 100% width |
| click | event | empty | Fires a click event |
