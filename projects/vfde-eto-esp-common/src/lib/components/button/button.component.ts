import { Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'vf-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})

export class Button {
    @Input() type: string;
    @Input() id: string;
    @Input() role: string;
    @Input() full_width: boolean;
    @Input() padding: boolean;
    @Output() onClick = new EventEmitter<any>();

    constructor() { }

    onClickButton(event) {
        this.onClick.emit(event);
    }

    buttons: any[] = [
        { "type": "primary" },
        { "type": "secondary" },
        { "type": "tertiary" },
        { "type": "plain" }
    ]
}
