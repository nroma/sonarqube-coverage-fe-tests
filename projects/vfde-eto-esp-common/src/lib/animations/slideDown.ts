import { animate, state, style, transition, trigger } from '@angular/animations';

export const slideDown = trigger('toggleState', [
    state('open', style({
        transform: 'translateY(0%)'
    })),
    state('closed', style({
        transform: 'translateY(0)',
        height: 0,
        'padding-top': 0,
        'padding-bottom': 0,
        display: 'none'
    })),
    transition('open => closed', [
        animate('500ms ease-out')
    ]),
    transition('closed => open', [
        animate('800ms cubic-bezier(0.600, 0.040, 0.980, 0.335)')
    ]),
])
