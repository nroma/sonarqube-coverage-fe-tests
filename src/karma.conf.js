// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html


// check if karma is runing inside docker

const isDocker = require('is-docker')();

module.exports = function (config) {
    config.set({
        basePath: '',
        frameworks: ['jasmine', '@angular-devkit/build-angular'],
        plugins: [
            require('karma-jasmine'),
            require('karma-chrome-launcher'),
            require('karma-jasmine-html-reporter'),
            require('karma-coverage-istanbul-reporter'),
            require('@angular-devkit/build-angular/plugins/karma')
        ],
        client: {
            clearContext: false // leave Jasmine Spec Runner output visible in browser
        },
        browsers: isDocker ? ['Chromium_no_sandbox'] : ['Chrome'],
        customLaunchers: {
            Chromium_no_sandbox: {
                base: 'ChromiumHeadless',
                flags: ['--no-sandbox']
            }
        },
        coverageIstanbulReporter: {
            dir: require('path').join(__dirname, '../coverage'),
            reports: ['html', 'lcovonly', 'text', 'text-summary'],
            fixWebpackSourcePaths: true,
            thresholds: {
                statements: 10,
                lines: 10,
                branches: 10,
                functions: 10
            },
            skipFilesWithNoCoverage: true,
            includeAllSources: true
        },
        reporters: ['progress', 'kjhtml'],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        singleRun:  isDocker ? true : false
    });
};
