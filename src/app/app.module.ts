import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClientXsrfModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Button, FormLabel, FormInput, Accordion, AccordionList, Panel, TableCell, Heading, TitleBlock, Text, TextInline, TextBold, Tooltip, TextLegalName, AlertBox, Dropdown } from 'vfde-eto-esp-common'; //TODO: Refactor call all
import { AuthenticationGuard } from '@services/authenticationRouteGuard.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
// Modules
import { AppRoutingModule } from '@modules/routing/routing.module';
import { Translate } from '@modules/translate/translate.module';
// Pages
import { LoginComponent } from '@pages/login/login.component';
import { DashboardComponent } from '@pages/dashboard/dashboard.component';
import { MySubscriptionsComponent } from '@pages/my-subscriptions/my-subscriptions.component';
import { MyAccountsComponent } from '@pages/my-accounts/my-accounts.component';
import { SimSwapComponent } from '@pages/sim-swap/sim-swap.component';
import { ShowPUKComponent } from '@pages/show-puk/show-puk.component';

// Services
import { AuthenticationService } from '@services/authentication.service';
import { AuthInterceptorService } from '@services/auth-interceptor.service';
// Components
import { Navigation } from '@components/navigation/navigation.component';

@NgModule({
    declarations: [
        AppComponent,
        DashboardComponent,
        LoginComponent,
        MySubscriptionsComponent,
        Navigation,
        MyAccountsComponent,
        SimSwapComponent,
        ShowPUKComponent,
        Accordion,
        AccordionList,
        AlertBox,
        Button,
        Dropdown,
        FormLabel,
        FormInput,
        Heading,
        Panel,
        TableCell,
        Text,
        TextInline,
        TextBold,
        TextLegalName,
        TitleBlock,
        Tooltip,
    ],
    imports: [
        AppRoutingModule,
        BrowserAnimationsModule,
        BrowserModule,
        CommonModule,
        HttpClientModule,
        HttpClientXsrfModule.withOptions({
            // TODO: cookie name to prevent colision
            // with other angular apps running on the same domain or subdomain
            cookieName: 'authToken',
            headerName: 'authToken',
        }),
        ReactiveFormsModule,
        Translate
    ],
    providers: [
        AuthenticationGuard,
        AuthenticationService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptorService,
            multi: true
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
