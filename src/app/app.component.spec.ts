import { TestBed, async } from '@angular/core/testing';
import { APP_BASE_HREF } from '@angular/common';
import { AppComponent } from './app.component';
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { TranslateLoader, TranslateModule, TranslateService } from "@ngx-translate/core";
import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// TODO:
// import {HttpLoaderFactory} from '@modules/translate.module';
import { HttpLoaderFactory } from './modules/translate/translate.module';

@Component({ selector: 'navigation', template: '' })
class Navigation { }


@Component({ selector: 'router-outlet', template: '' })
class RouterOutletStubComponent { }

const TRANSLATIONS_DE = require('./assets/i18n/de.json');

describe('AppComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent,
                Navigation,
                RouterOutletStubComponent,
            ],
            providers: [
                TranslateService,
                { provide: APP_BASE_HREF, useValue: '/' }
            ],
            imports: [
                HttpClientTestingModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: HttpLoaderFactory,
                        deps: [HttpClient]
                    }
                })
            ],

        }).compileComponents();
    }));

    it('should create the app', async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));
});
