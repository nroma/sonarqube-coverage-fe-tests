import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { AuthInterceptorService } from './auth-interceptor.service';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

describe('AuthInterceptorService', () => {
    let component: AuthInterceptorService;
    let router = {
        navigate: jasmine.createSpy('navigate')
    }

    beforeEach(() => {
        TestBed.configureTestingModule({
            // Import the HttpClient mocking services
            imports: [HttpClientTestingModule, RouterTestingModule],
            // Provide the service-under-test
            providers: [AuthInterceptorService, { provide: Router, useValue: router }]
        }).compileComponents();

        component = TestBed.get(AuthInterceptorService);
    });

    it('should catch error', () => {
        component.handleAuthError();
        expect(router.navigate).toHaveBeenCalledWith(["login"]);
    });
});
