import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

// Other imports
import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { ShowPUKService } from './puk.service';

describe('ShowPUKService', () => {
    let httpClient: HttpClient;
    let httpTestingController: HttpTestingController;
    let showPUKService: ShowPUKService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            // Import the HttpClient mocking services
            imports: [HttpClientTestingModule],
            // Provide the service-under-test
            providers: [ShowPUKService]
        });

        // Inject the http, test controller, and service-under-test
        // as they will be referenced by each test.
        httpClient = TestBed.get(HttpClient);
        httpTestingController = TestBed.get(HttpTestingController);
        showPUKService = TestBed.get(ShowPUKService);
    });
    //TODO: test server response with stubs. S
    it('should be created', () => {
        const service: ShowPUKService = TestBed.get(ShowPUKService);
        expect(service).toBeTruthy();
    });
});
