import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { environment } from '@environments/environment';

@Injectable({
    providedIn: 'root'
})


export class AccountService {
    public static readonly GET_ACCOUNTS_SUB_PATH = '/api/accounts';
    baseUrl: string = environment.baseUrl;

    constructor(private http: HttpClient) { }
    public getAccounts() {
        return this.http.post(this.baseUrl + AccountService.GET_ACCOUNTS_SUB_PATH, { observe: 'response' });
    }
}
