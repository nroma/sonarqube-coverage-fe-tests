import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { SubscriptionService } from '@services/subscription.service';
import { environment } from '@environments/environment';

interface Subscriptions {
    name: string;
}

const testUrl = '/api/subscriptions';
const BaseUrl = environment.baseUrl;


describe('SubscriptionService', () => {
    let httpTestingController: HttpTestingController;
    let subscriptionService: SubscriptionService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            // Import the HttpClient mocking services
            imports: [HttpClientTestingModule],
            // Provide the service-under-test
            providers: [
                SubscriptionService
            ]
        });

        // Inject the http, test controller, and service-under-test
        // as they will be referenced by each test.
        httpTestingController = TestBed.get(HttpTestingController);
        subscriptionService = TestBed.get(SubscriptionService);
    });
    afterEach(() => {
        // After every test, assert that there are no more pending requests.
        httpTestingController.verify();
    });
    it('should be created', () => {
        // const service: SubscriptionService = TestBed.get(SubscriptionService);
        expect(subscriptionService).toBeDefined();
    });

    /// Tests begin ///
    it('#getSubsbcriptions should return an observable', () => {
        const testData: Subscriptions = { name: 'Test Data' };

        // Make an HTTP POST request
        subscriptionService.getSubscriptions()
            .subscribe(data =>
                // When observable resolves, result should match test data
                expect(data).toEqual(testData)
            );

        // The following `expectOne()` will match the request's URL.
        // If no requests or multiple requests matched that URL
        // `expectOne()` would throw.
        const req = httpTestingController.expectOne(BaseUrl + SubscriptionService.SUBSCRIPTIONS_URL);
        // Assert that the request is a POST.
        expect(req.request.method).toEqual('POST');

        // Respond with mock data, causing Observable to resolve.
        // Subscribe callback asserts that correct data was returned.
        req.flush(testData);

        // Finally, assert that there are no outstanding requests.
        httpTestingController.verify();
    });

    it('#SubsbcriptionsURL should be well know', () => {
        // Test to ensure that changes in url are noticed
        expect(testUrl).toEqual(SubscriptionService.SUBSCRIPTIONS_URL);
    });

});
