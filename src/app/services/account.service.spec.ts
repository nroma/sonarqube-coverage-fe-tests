import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

// Other imports
import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { AccountService } from './account.service';

describe('AccountService', () => {
    let httpClient: HttpClient;
    let httpTestingController: HttpTestingController;
    let accountService: AccountService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            // Import the HttpClient mocking services
            imports: [HttpClientTestingModule],
            // Provide the service-under-test
            providers: [AccountService]
        });

        // Inject the http, test controller, and service-under-test
        // as they will be referenced by each test.
        httpClient = TestBed.get(HttpClient);
        httpTestingController = TestBed.get(HttpTestingController);
        accountService = TestBed.get(AccountService);
    });
    //TODO: test server response with stubs. S
    it('should be created', () => {
        const service: AccountService = TestBed.get(AccountService);
        expect(service).toBeTruthy();
    });
});
