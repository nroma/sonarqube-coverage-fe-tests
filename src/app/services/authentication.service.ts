import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '@environments/environment';


@Injectable()
export class AuthenticationService {
    public static readonly LOGIN_URL = '/api/login';
    public static readonly KEY_LOGGED = 'logged';
    private isLoggedin = false;
    baseUrl: string = environment.baseUrl;

    constructor(private http: HttpClient, private router: Router) { }

    public isAuthenticated(): boolean {
        // TODO: how do we check in the front end if the user has access to certain routs?
        // 1-Simple boolean in localtorage?
        // 2- Whait for resquest to server that will fail if user does not have permitions?
        // 3-timer?
        this.isLoggedin = JSON.parse(localStorage.getItem(AuthenticationService.KEY_LOGGED));
        return this.isLoggedin;
    }

    private getToken(loginForm) {
        return this.http.post(this.baseUrl + AuthenticationService.LOGIN_URL, loginForm, { observe: 'response' });
    }

    /**
     * login
     */
    public login(loginForm) {
        if (loginForm) {
            this.getToken(loginForm).subscribe(() => {
                this.router.navigate(['dashboard']);
            });
            this.isLoggedin = true;
            localStorage.setItem(AuthenticationService.KEY_LOGGED, JSON.stringify(this.isLoggedin));
        }
    }
}
