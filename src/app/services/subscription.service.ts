import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';

@Injectable({
    providedIn: 'root'
})


export class SubscriptionService {
    public static readonly SUBSCRIPTIONS_URL = '/api/subscriptions';
    baseUrl: string = environment.baseUrl;

    constructor(private http: HttpClient) { }
    public getSubscriptions() {
        return this.http.post(this.baseUrl + SubscriptionService.SUBSCRIPTIONS_URL, { observe: 'response' });
    }
}
