import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { AuthenticationService } from './authentication.service';
import { RouterTestingModule } from '@angular/router/testing';
import { FormGroup, FormControl } from '@angular/forms';

const testUrl = '/api/login';

describe('AuthenticationService', () => {
    let authenticationService: AuthenticationService;
    beforeEach(() => {
        // mock localStorage
        let store = {};
        const mockLocalStorage = {
            getItem: (key: string): string => {
                return key in store ? store[key] : null;
            },
            setItem: (key: string, value: string) => {
                store[key] = `${value}`;
            },
            removeItem: (key: string) => {
                delete store[key];
            },
            clear: () => {
                store = {};
            }
        };
        // spy on localStorage invocation and replace with mockStorage
        spyOn(localStorage, 'getItem')
            .and.callFake(mockLocalStorage.getItem);
        spyOn(localStorage, 'setItem')
            .and.callFake(mockLocalStorage.setItem);
        spyOn(localStorage, 'removeItem')
            .and.callFake(mockLocalStorage.removeItem);
        spyOn(localStorage, 'clear')
            .and.callFake(mockLocalStorage.clear);

        TestBed.configureTestingModule({
            // Import the HttpClient mocking services
            imports: [HttpClientTestingModule, RouterTestingModule],
            // Provide the service-under-test
            providers: [AuthenticationService]
        });
        authenticationService = TestBed.get(AuthenticationService);
    });
    it('should be created', () => {
        const service: AuthenticationService = TestBed.get(AuthenticationService);
        expect(service).toBeTruthy();
    });

    /// Tests begin ///
    it('#login should add a token in local storage if loginForm.value is defined and user has valid credential', () => {
        const loginForm = new FormGroup({
            username: new FormControl('user1'),
            password: new FormControl('123456'),
        });
        expect(authenticationService.isAuthenticated()).toBeFalsy();
        expect(JSON.parse(localStorage.getItem(AuthenticationService.KEY_LOGGED))).toBeNull();

        authenticationService.login(loginForm.value);
        expect(authenticationService.isAuthenticated()).toBeTruthy();
        expect(JSON.parse(localStorage.getItem(AuthenticationService.KEY_LOGGED))).toBeTruthy();

        // TODO: Should navigate to dashboard page after sucessfull loggin

    });

    it('#login User should not be loggedin if loginForm.value is undefined and user has invalid credential', () => {
        expect(authenticationService.isAuthenticated()).toBeFalsy();
        expect(JSON.parse(localStorage.getItem(AuthenticationService.KEY_LOGGED))).toBeNull();

        authenticationService.login(undefined);
        expect(authenticationService.isAuthenticated()).toBeFalsy();
        expect(JSON.parse(localStorage.getItem(AuthenticationService.KEY_LOGGED))).toBeNull();

        authenticationService.login(null);
        expect(authenticationService.isAuthenticated()).toBeFalsy();
        expect(JSON.parse(localStorage.getItem(AuthenticationService.KEY_LOGGED))).toBeNull();

    });

    it('#SubsbcriptionsURL should be well know', () => {
        // Test to ensure that changes in url are noticed
        expect(testUrl).toEqual(AuthenticationService.LOGIN_URL);
    });
});
