import { HttpClientTestingModule } from '@angular/common/http/testing';

// Other imports
import { TestBed } from '@angular/core/testing';
import { AuthenticationGuard } from './authenticationRouteGuard.service';
import { AuthenticationService } from './authentication.service';
import { RouterTestingModule } from '@angular/router/testing';

class MockRouter {
    navigate(path) { }
}

describe('AuthenticationGuard', () => {

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule, RouterTestingModule],
            // Provide the service-under-test
            providers: [AuthenticationGuard, AuthenticationService]
        });
    });

    describe('canActivate', () => {
        let authGuard: AuthenticationGuard;
        let authService;
        let router;

        it('should return true for a logged in user', () => {
            authService = { isAuthenticated: () => true };
            router = new MockRouter();
            authGuard = new AuthenticationGuard(authService, router);

            expect(authGuard.canActivate()).toEqual(true);
        });

        it('should navigate to login for a non logged user', () => {
            authService = { isAuthenticated: () => false };
            router = new MockRouter();
            authGuard = new AuthenticationGuard(authService, router);
            spyOn(router, 'navigate');

            expect(authGuard.canActivate()).toEqual(false);
            expect(router.navigate).toHaveBeenCalledWith(['login']);
        });
    });
});
