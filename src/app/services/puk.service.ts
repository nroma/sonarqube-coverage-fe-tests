import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';

@Injectable({
    providedIn: 'root'
})


export class ShowPUKService {
    public static readonly GET_ACCOUNTS_SUB_PATH = '/api/puk';
    baseUrl: string = environment.baseUrl;

    constructor(private http: HttpClient) { }
    public getPuks(msisdn) {
        var bodyObj = { 'subscriptionId': msisdn }
        return this.http.post(this.baseUrl + ShowPUKService.GET_ACCOUNTS_SUB_PATH, bodyObj, { observe: 'body' });
    }
}
