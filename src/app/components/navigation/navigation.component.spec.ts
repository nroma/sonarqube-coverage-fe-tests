import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Navigation } from './navigation.component';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { HttpLoaderFactory } from '@modules/translate/translate.module';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';


describe('NavigationComponent', () => {
    let component: Navigation;
    let fixture: ComponentFixture<Navigation>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [Navigation],
            imports: [
                HttpClientTestingModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: HttpLoaderFactory,
                        deps: [HttpClient]
                    }
                })
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(Navigation);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeDefined();
    });
});
