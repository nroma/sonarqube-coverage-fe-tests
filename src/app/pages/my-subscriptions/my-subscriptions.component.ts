import { Component, OnInit } from '@angular/core';
import { SubscriptionService } from '@services/subscription.service';

@Component({
    selector: 'app-my-subscriptions',
    templateUrl: './my-subscriptions.component.html',
    styleUrls: ['./my-subscriptions.component.scss']
})
export class MySubscriptionsComponent implements OnInit {
    accounts;

    constructor(private subscriptionService: SubscriptionService) { }

    ngOnInit() {
        this.getSubscriptions();
    }

    getSubscriptions() {
        this.subscriptionService.getSubscriptions().subscribe(resp => {
            // TODO: this will be changed after changes in the backend.
            this.accounts = resp;
        });
    }
}
