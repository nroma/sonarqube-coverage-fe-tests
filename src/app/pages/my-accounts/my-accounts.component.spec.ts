import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MyAccountsComponent } from './my-accounts.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { HttpLoaderFactory } from '@modules/translate/translate.module';
import { AuthenticationService } from '@services/authentication.service';

describe('MyAccountsComponent', () => {
    let component: MyAccountsComponent;
    let fixture: ComponentFixture<MyAccountsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [MyAccountsComponent],
            imports: [
                HttpClientTestingModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: HttpLoaderFactory,
                        deps: [HttpClient]
                    }
                })
            ],
            schemas: [
                CUSTOM_ELEMENTS_SCHEMA
            ],
            providers: [AuthenticationService]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MyAccountsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeDefined();
    });
});
