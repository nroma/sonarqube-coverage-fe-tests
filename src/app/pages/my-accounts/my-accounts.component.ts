import { Component, OnInit } from '@angular/core';
import { AccountService } from '@services/account.service';

@Component({
    selector: 'app-my-accounts',
    templateUrl: './my-accounts.component.html',
    styleUrls: ['./my-accounts.component.scss']
})
export class MyAccountsComponent implements OnInit {
    accounts;
    constructor(private accountService: AccountService) { }

    ngOnInit() {
        this.getAccounts();
    }

    getAccounts() {
        this.accountService.getAccounts().subscribe(resp => {
            // TODO: this will be changed after changes in the backend.
            this.accounts = resp;
        });
    }
}
