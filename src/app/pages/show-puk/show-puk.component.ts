import { Component, OnInit } from '@angular/core';
import { AccountService } from '@services/account.service';
import { ShowPUKService } from '@services/puk.service';

@Component({
    selector: 'app-show-puk',
    templateUrl: './show-puk.component.html',
    styleUrls: ['./show-puk.component.scss']
})
export class ShowPUKComponent implements OnInit {
    accounts;
    puk;
    toggle;

    constructor(
        private accountService: AccountService,
        private pukService: ShowPUKService) { }

    ngOnInit() {
        this.getAccounts();
    }

    getAccounts() {
        this.accountService.getAccounts().subscribe((resp: any) => {
            console.log(resp);
            this.accounts = resp;
        });
    }

    getPuk(msisdn: number) {
        this.pukService.getPuks(msisdn).subscribe((resp: any) => {
            this.puk = resp.puk;
            this.toggle = msisdn;
        });
    }

    getOnboarded(object: object) {
        let subscriptionsIndex = Object.keys(object).findIndex(y => Object.is('subscriptions', y))
        let subscriptions = Object.values(object)[subscriptionsIndex];
        for (let i = 0; i < subscriptions.length; i++) {
            if (subscriptions[i].onboarded) {
                return true;
            }
        }
        return false;
    }
}
