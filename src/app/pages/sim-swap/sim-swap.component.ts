import { Component, OnInit } from '@angular/core';
import { AccountService } from '@services/account.service';

@Component({
    selector: 'app-sim-swap',
    templateUrl: './sim-swap.component.html',
    styleUrls: ['./sim-swap.component.scss']
})

export class SimSwapComponent implements OnInit {
    accounts;
    simCards = [];

    constructor(private accountService: AccountService) { }

    ngOnInit() {
        this.getAccounts();
    }

    getAccounts() {
        this.accountService.getAccounts().subscribe((resp: any) => {
            this.accounts = resp;
        });
    }

    getSimList(simCards: Array<object>) {
        let dropdownValues = [];
        simCards.forEach((simCard: object) => {
            let label = JSON.stringify(simCard["type"]);
            let value = JSON.stringify(simCard["simId"]);
            label = JSON.parse(label.replace("type", "label"));
            value = JSON.parse(value.replace("simId", "value"));
            dropdownValues.push({ label, value });
        });
        return dropdownValues;
    }

    getOnboarded(object: object) {
        let subscriptionsIndex = Object.keys(object).findIndex(y => Object.is('subscriptions', y))
        let subscriptions = Object.values(object)[subscriptionsIndex];
        for (let i = 0; i < subscriptions.length; i++) {
            if (subscriptions[i].onboarded) {
                return true;
            }
        }
        return false;
    }
}
