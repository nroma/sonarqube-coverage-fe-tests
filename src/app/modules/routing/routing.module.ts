import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard } from '@services/authenticationRouteGuard.service';
// pages
import { LoginComponent } from '@pages/login/login.component';
import { DashboardComponent } from '@pages/dashboard/dashboard.component';
import { MySubscriptionsComponent } from '@pages/my-subscriptions/my-subscriptions.component';
import { MyAccountsComponent } from '@pages/my-accounts/my-accounts.component';
import { SimSwapComponent } from '@pages/sim-swap/sim-swap.component';
import { ShowPUKComponent } from '@pages/show-puk/show-puk.component';

const routes: Routes = [
    { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    { path: 'dashboard', component: DashboardComponent },
    { path: 'mySubscriptions', component: MySubscriptionsComponent, canActivate: [AuthenticationGuard] },
    { path: 'myAccounts', component: MyAccountsComponent, canActivate: [AuthenticationGuard] },
    { path: 'myAccounts/simSwap', component: SimSwapComponent, canActivate: [AuthenticationGuard] },
    { path: 'myAccounts/showPUK', component: ShowPUKComponent, canActivate: [AuthenticationGuard] }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule { }
