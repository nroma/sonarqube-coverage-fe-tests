#!/bin/bash

sleep 20
ceTaskUrl=$(grep -o 'http://sonarqube:9000/api[^"]*' .scannerwork/report-task.txt)
curl $ceTaskUrl -o ceTask.json
analysisId=$( grep -o '"analysisId": *"[^"]*"' ceTask.json | grep -o '"[^"]*"$' | tr -d '"')
curl http://sonarqube:9000/api/qualitygates/project_status?analysisId=$analysisId -o qualityGate.json
if grep -o 'projectStatus":{"status":"OK",' qualityGate.json; then
 result=0
 echo "Sonar Status OK"
else
 result=1
 echo "Sonar Status Not OK"
fi
exit $result